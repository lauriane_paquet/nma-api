#!/usr/bin/env python3

import unittest
import sys
import numpy as np
from Bio.PDB import *
sys.path.insert(0, '../src')
import molecule as mol


class TestMolecule(unittest.TestCase):
    def test_init(self):
        prot = mol.Molecule()
        self.assertIsNone(prot.correlation)
        self.assertIsNone(prot.pdb_protein)

        prot = mol.Molecule()
        prot.init_protein("data/pdb_test_10_atoms_line.pdb")
        self.assertIsNotNone(prot.pdb_protein)

    def test_init_protein_and_delete_hetatm(self):
        nfile = "data/pdb_test_10_atoms_line.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        self.assertEqual(10, len([atom for atom in Selection.unfold_entities(prot.pdb_protein, target_level='A')]))
        self.assertEqual("data", prot.data_path)
        # seen clean_pdb_test_10_atoms_line.pdb : cleaned up alright

        atoms = [[0, 0, 0], [1, 1, 1], [2, 2, 2], [3, 3, 3], [4, 4, 4],
                 [5, 5, 5], [6, 6, 6], [7, 7, 7], [8, 8, 8], [9, 9, 9]]
        num_atoms = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        serial_numbers = [atom.serial_number for atom in Selection.unfold_entities(prot.pdb_protein, target_level='A')]
        prot_dict = dict(zip(serial_numbers, Selection.unfold_entities(prot.pdb_protein, target_level='A')))
        for i in num_atoms:
            np.testing.assert_array_equal(atoms[i - 1], prot_dict[serial_numbers[i - 1]].get_coord())

        nfile = "data/pdb_test_empty.pdb"
        prot = mol.Molecule()
        self.assertEqual(1, prot.init_protein(nfile))

        nfile = "data/1e15.cif"
        prot = mol.Molecule()
        self.assertEqual(0, prot.init_protein(nfile))

    def test_init_correlation(self):
        prot = mol.Molecule()
        prot.init_correlation("data/matrix_10x10_symmetrical.dat")
        matrix = np.zeros((10, 10), dtype=float)
        matrix[0, 0] = -0.1
        matrix[0, 9] = matrix[9, 0] = .3
        matrix[3, 7] = matrix[7, 3] = .4
        np.testing.assert_array_equal(matrix, prot.correlation.corr_matrix, "test matrix read well")

        prot = mol.Molecule()
        prot.init_protein("data/pdb_test_2_atoms.pdb")
        self.assertEqual(0, prot.init_correlation(), "assert take good file automatically")
        np.testing.assert_array_equal([[-0.8, 0.4], [0.4, -0.8]], prot.correlation.corr_matrix, "test matrix read well")

    def test_get_atoms_serial_number(self):
        nfile = "data/pdb_test_10_atoms_line.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        np.testing.assert_array_equal([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], prot.get_atoms_serial_number())
        np.testing.assert_array_equal([1, 2, 3, 4, 5, 6, 7, 8], prot.get_atoms_serial_number('A'))

        nfile = "data/1e15.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        self.assertEqual(992, len(prot.get_atoms_serial_number(chains='', atom_type='CA')))
        self.assertEqual(496, len(prot.get_atoms_serial_number(chains='A', atom_type='CA')))
        self.assertEqual(992, len(prot.get_atoms_serial_number(chains='[A, B]', atom_type='CA')))
        self.assertEqual(906, len(prot.get_atoms_serial_number(atom_type='CB')))

    def test_hide_close_pairs_atoms(self):
        nfile = "data/pdb_test_10_atoms_line.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        prot.init_correlation("data/matrix_10x10_symmetrical.dat")
        prot.hide_close_pairs_atoms()
        for i in range(10):
            for j in range(10):
                if j == i:
                    self.assertEqual(float('Inf'), prot.correlation.corr_matrix[i, j], "diag == inf")
                else:
                    self.assertNotEqual(float('Inf'), prot.correlation.corr_matrix[i, j], "not diag != inf")

        prot.hide_close_pairs_atoms(3.1)
        matrix = np.zeros((10, 10), dtype=float)
        matrix[0, 0] = -0.1
        matrix[0, 9] = matrix[9, 0] = .3
        matrix[3, 7] = matrix[7, 3] = .4
        for i in range(10):
            for j in range(i, 10):
                tmp = (float(i) - float(j)) * (float(i) - float(j)) * 3.1
                if np.sqrt(tmp) < 3:
                    matrix[i, j] = matrix[j, i] = float('Inf')
        np.testing.assert_array_equal(matrix, prot.correlation.corr_matrix)

        prot.hide_close_pairs_atoms(8)
        matrix = np.zeros((10, 10), dtype=float)
        matrix[0, 0] = -0.1
        matrix[0, 9] = matrix[9, 0] = .3
        matrix[3, 7] = matrix[7, 3] = .4
        for i in range(10):
            for j in range(i, 10):
                tmp = (float(i) - float(j)) * (float(i) - float(j)) * 3
                if np.sqrt(tmp) < 8:
                    matrix[i, j] = matrix[j, i] = float('Inf')
        np.testing.assert_array_equal(matrix, prot.correlation.corr_matrix)

    def test_get_pairs_atoms_infos(self):
        nfile = "data/1e15.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        prot.init_correlation("data/1e15_A_CA_correlation_matrix.dat")
        val = [.1, .1]
        self.assertEqual(sum(len(x) for x in
                             prot.correlation.pairs_atoms_threshold_percent_abs_selection(val[0], val[1])),
                         sum(len(x) for x in
                             prot.get_pairs_atoms_infos(chains_used=[], atoms_used='CA', selection=2,
                                                        threshold_upper=val[0], threshold_lower=val[1])[1:]),
                         "assert get_pairs_atoms_infos get same pairs as it should")
        val = [1, 1]
        self.assertEqual(sum(len(x) for x in
                             prot.correlation.pairs_atoms_threshold_percent_selection(val[0], val[1])),
                         sum(len(x) for x in
                             prot.get_pairs_atoms_infos(chains_used=[], atoms_used='CA', selection=1,
                                                        threshold_upper=val[0], threshold_lower=val[1])[1:]),
                         "assert get_pairs_atoms_infos get same pairs as it should")
        val = [-1, -.4]
        prot.correlation.change_threshold(val[0], val[1])
        self.assertEqual(sum(len(x) for x in
                             prot.correlation.pairs_atoms_threshold_selection()),
                         sum(len(x) for x in
                             prot.get_pairs_atoms_infos(chains_used=[], atoms_used='CA', selection=0,
                                                        threshold_upper=val[0], threshold_lower=val[1])[1:]),
                         "assert get_pairs_atoms_infos get same pairs as it should")

        prot.hide_close_pairs_atoms(8)
        val = [.05, .05]
        self.assertEqual(sum(len(x) for x in
                             prot.correlation.pairs_atoms_threshold_percent_abs_selection(val[0], val[1])),
                         sum(len(x) for x in
                             prot.get_pairs_atoms_infos(chains_used=[], atoms_used='CA', selection=2,
                                                        threshold_upper=val[0], threshold_lower=val[1])[1:]),
                         "assert get_pairs_atoms_infos get same pairs as it should with some hidden values")

    def test_write_correlation_sticks_pml(self):
        nfile = "data/1e15.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        prot.init_correlation("data/1e15_A_CA_correlation_matrix.dat")
        prot.hide_close_pairs_atoms(8)
        prot.write_correlation_sticks_pml(prot.get_pairs_atoms_infos(
            chains_used=['A'], atoms_used='CA', selection=2, threshold_upper=.01, threshold_lower=.01))

        nfile = "data/pdb_test_10_atoms_line.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        prot.init_correlation("data/matrix_10x10_symmetrical.dat")
        prot.hide_close_pairs_atoms()
        prot.write_correlation_sticks_pml(prot.get_pairs_atoms_infos(
            chains_used=['A'], atoms_used='CA', selection=2, threshold_upper=.01, threshold_lower=.01))
        prot.hide_close_pairs_atoms(8)
        prot.write_correlation_sticks_pml(prot.get_pairs_atoms_infos(
            chains_used=['A'], atoms_used='CA', selection=2, threshold_upper=0, threshold_lower=0))
        prot.write_correlation_sticks_pml(prot.get_pairs_atoms_infos(
            chains_used=[], atoms_used='CA', selection=2, threshold_upper=1, threshold_lower=1))
        # seen in pymol and file out; everything ok

        nfile = "data/1e15.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        prot.init_correlation("data/1e15_correlation_matrix.dat")
        prot.hide_close_pairs_atoms(8)
        prot.write_correlation_sticks_pml(prot.get_pairs_atoms_infos(
            atoms_used='CA', selection=1, threshold_upper=.01, threshold_lower=.01))
        prot.write_correlation_sticks_pml(prot.get_pairs_atoms_infos(
            atoms_used='CA', selection=0, threshold_upper=.01, threshold_lower=.01))
        # take into account right chain ok (tested with 2 chains. Results are ok).

    def test_calculate_secondary_structures(self):
        nfile = "data/1e15.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        dssp_result_path = prot.data_path.split("/")[:-1]
        dssp_result_path.append("results/")
        dssp_result_path = "/".join(dssp_result_path)
        dssp_result_path += "_".join(prot.pdb_protein.get_id().split("_")[:-1]) + "_DSSP" + ".dat"

        self.assertEqual(0, prot.calculate_secondary_structures(dssp_result_path))

    def test_read_dssp_file(self):
        nfile = "data/1e15.pdb"
        prot = mol.Molecule()
        prot.init_protein(nfile)
        dssp_result_path = prot.data_path.split("/")[:-1]
        dssp_result_path.append("results/")
        dssp_result_path = "/".join(dssp_result_path)
        dssp_result_path += "_".join(prot.pdb_protein.get_id().split("_")[:-1]) + "_DSSP" + ".dat"

        self.assertEqual(0, prot.calculate_secondary_structures(dssp_result_path))
        second_struct = prot.read_dssp_file(dssp_result_path)
        self.assertNotEqual(None, second_struct)
        struct_types = []
        for i in second_struct:
            struct_types.append(i[0][0])
        self.assertEqual(16,   len(second_struct[struct_types.index('G')]) - 1)
        self.assertEqual(30,   len(second_struct[struct_types.index('H')]) - 1)
        self.assertEqual(0,    len(second_struct[struct_types.index('I')]) - 1)
        self.assertEqual(69,   len(second_struct[struct_types.index('T')]) - 1)
        self.assertEqual(34,   len(second_struct[struct_types.index('E')]) - 1)
        self.assertEqual(20,   len(second_struct[struct_types.index('B')]) - 1)
        self.assertEqual(64,   len(second_struct[struct_types.index('S')]) - 1)
        self.assertEqual(0,    len(second_struct[struct_types.index('C')]) - 1)


if __name__ == '__main__':
    unittest.main()
