#!/usr/bin/env python3

import unittest
import sys
sys.path.insert(0, '../src')
import modes


class TestModes(unittest.TestCase):
    def test_init(self):
        return 0

    def test_read_modes_webnma_file_pos(self):
        mode = modes.Modes(modes_file="data/1e15_A_modes.dat")
        mode.read_modes_webnma_file()
        self.assertEqual("data/1e15_A_modes.dat", mode.modes_file, "assert good file used")
        self.assertEqual(1200, len(mode.eigenvalues), "assert good number of eigenvalues")
        self.assertEqual(len(mode.eigenvalues), len(list(mode.modes.keys())),
                         "assert number of eigenvalues equal to number of modes")

        mode = modes.Modes(modes_file="data/mode_5res_10modes.dat")
        mode.read_modes_webnma_file()
        self.assertEqual("data/mode_5res_10modes.dat", mode.modes_file, "assert good file used")
        self.assertEqual(10, len(mode.eigenvalues), "assert good number of eigenvalues")
        self.assertEqual(len(mode.eigenvalues), len(list(mode.modes.keys())),
                         "assert number of eigenvalues equal to number of modes")
        self.assertEqual(5, len(mode.aa), "assert number of atoms ok")
        for i in mode.modes.keys():
            self.assertEqual(len(mode.aa), len(mode.modes[i].keys()),
                             "assert number of atoms equal to atoms stocked")
        k = 10
        for i in mode.modes.keys():
            for j in mode.aa:
                self.assertEqual(mode.modes[i][j][0], mode.modes[i][j][1], "assert xyz same")
                self.assertEqual(mode.modes[i][j][1], mode.modes[i][j][2], "assert xyz same")
                self.assertEqual(k, mode.modes[i][j][0], "assert values ok")
            if k > 0:
                k -= 1
            if k < 0:
                k += 1
            elif k == 4:
                k = -k

        mode = modes.Modes(modes_file="data/mode_5res_10-9modes.dat")
        self.assertEqual(1, mode.read_modes_webnma_file())

        mode = modes.Modes(modes_file="data/mode_5-6res_10modes.dat")
        self.assertEqual(1, mode.read_modes_webnma_file())

    def test_calc_correlation(self):
        mode = modes.Modes(modes_file="data/1e15_A_modes.dat")
        mode.read_modes_webnma_file()
        mode.calc_correlation()

    def test_calc_fluctuation(self):
        mode = modes.Modes(modes_file="data/1e15_A_modes.dat")
        mode.read_modes_webnma_file()


if __name__ == '__main__':
    unittest.main()
