#!/usr/bin/env python3

import unittest
import sys
import numpy as np
sys.path.insert(0, '../src')
import correlation_matrix as cs


class TestCorrelationMatrix(unittest.TestCase):
    def test_init(self):
        cor = cs.CorrelationMatrix(threshold_upper=.5, dist_min=1)
        self.assertEqual(.5, cor.threshold_upper, "threshold_upper should be changed")
        self.assertEqual(-.9, cor.threshold_lower, "threshold_lower should be default")
        self.assertEqual(1, cor.dist_min, "dist_min should not be default")

    def test_read_correlation_matrix_file(self):
        cor = cs.CorrelationMatrix("data/matrix_empty.dat", threshold_upper=.5, dist_min=1)
        cor.read_correlation_matrix_file()
        self.assertEqual(1, cor.read_correlation_matrix_file(), "test if matrix is empty")

        cor = cs.CorrelationMatrix("data/matrix_10x4.dat", threshold_upper=.5, dist_min=1)
        self.assertEqual(1, cor.read_correlation_matrix_file(), "test if matrix is square")

        cor = cs.CorrelationMatrix("data/matrix_10x10_symmetrical.dat", threshold_upper=.5, dist_min=1)
        self.assertEqual(0, cor.read_correlation_matrix_file(), "test matrix read if square")
        matrix = np.zeros((10, 10), dtype=float)
        matrix[0, 0] = -0.1
        matrix[0, 9] = matrix[9, 0] = .3
        matrix[3, 7] = matrix[7, 3] = .4
        np.testing.assert_array_equal(matrix, cor.corr_matrix, "test matrix read well")

    def test_test_matrix_symmetry(self):
        cor = cs.CorrelationMatrix("data/matrix_10x10_symmetrical.dat", threshold_upper=.5, dist_min=1)
        self.assertEqual(0, cor.test_matrix_symmetry(), "positive test")

        cor = cs.CorrelationMatrix("data/matrix_10x10_asymmetrical.dat", threshold_upper=.5, dist_min=1)
        self.assertEqual(1, cor.read_correlation_matrix_file(), "negative test (read_correlation_matrix_file call test_"
                                                                "matrix_symmetry and delete matrix if not symmetrical)")

    def test_change_threshold(self):
        cor = cs.CorrelationMatrix("data/matrix_10x10_symmetrical.dat", threshold_upper=.5, dist_min=1)
        values_ok = [.987654321234567, -0]
        cor.change_threshold(values_ok[0], values_ok[1])
        self.assertEqual(values_ok[0], cor.threshold_upper, "test normal values - threshold upper")
        self.assertEqual(values_ok[1], cor.threshold_lower, "test normal values - threshold lower")

        values_oob = [-1.987654321234567, 1]
        cor.change_threshold(values_oob[0], values_oob[1])
        self.assertEqual(values_ok[0], cor.threshold_upper, "test out of bonds values - threshold upper")
        self.assertEqual(values_ok[1], cor.threshold_lower, "test out of bonds values - threshold lower")

    def test_change_dist_min(self):
        cor = cs.CorrelationMatrix("data/matrix_10x10_symmetrical.dat", threshold_upper=.5, dist_min=1)
        value_ok = .987654321234567
        cor.change_dist_min(value_ok)
        self.assertEqual(value_ok, cor.dist_min, "test normal value")

        value_ok = -0
        cor.change_dist_min(value_ok)
        self.assertEqual(value_ok, cor.dist_min, "test normal value")

        value_ok = 2345678900987654
        cor.change_dist_min(value_ok)
        self.assertEqual(value_ok, cor.dist_min, "test normal value")

        value_oob = -80
        self.assertEqual(1, cor.change_dist_min(value_oob), "test out of bond value")
        self.assertEqual(value_ok, cor.dist_min, "test normal value was kept")

    def test_pairs_atoms_threshold_selection(self):
        cor = cs.CorrelationMatrix("data/matrix_10x10_symmetrical.dat", threshold_upper=.5, dist_min=1)
        cor.change_threshold(.4, -.1)
        np.testing.assert_array_equal([[(4, 8)], []], cor.pairs_atoms_threshold_selection(),
                                      "test select pairs ok (>.4 & <-.1 out of diag)")

        cor.change_threshold(.404, .3)
        np.testing.assert_array_equal([[], [(1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (1, 8), (1, 9), (1, 10),
                                            (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (2, 10),
                                            (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10),
                                            (4, 5), (4, 6), (4, 7), (4, 9), (4, 10),
                                            (5, 6), (5, 7), (5, 8), (5, 9), (5, 10),
                                            (6, 7), (6, 8), (6, 9), (6, 10),
                                            (7, 8), (7, 9), (7, 10),
                                            (8, 9), (8, 10),
                                            (9, 10)]], cor.pairs_atoms_threshold_selection(),
                                      "test select pairs ok (>.404 & <.3 out of diag)")

        cor.change_threshold(float('Inf'), -float('Inf'))
        np.testing.assert_array_equal([[], []], cor.pairs_atoms_threshold_selection(),
                                      "test select pairs ok (>Inf & <-Inf out of diag)")

    def test_pairs_atoms_threshold_percent_selection(self):
        cor = cs.CorrelationMatrix("data/matrix_10x10_symmetrical.dat", threshold_upper=.5, dist_min=1)
        np.testing.assert_array_equal([[(4, 8)], []], cor.pairs_atoms_threshold_percent_selection(.01, .01),
                                      "test select pairs ok")

        np.testing.assert_array_equal([[], []], cor.pairs_atoms_threshold_percent_selection(0, 0), "test select nothing")
        np.testing.assert_array_equal([[(1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (1, 8), (1, 9), (1, 10),
                                        (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (2, 10),
                                        (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10),
                                        (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10),
                                        (5, 6), (5, 7), (5, 8), (5, 9), (5, 10),
                                        (6, 7), (6, 8), (6, 9), (6, 10),
                                        (7, 8), (7, 9), (7, 10),
                                        (8, 9), (8, 10),
                                        (9, 10)],
                                       [(1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (1, 8), (1, 9), (1, 10),
                                        (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (2, 10),
                                        (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10),
                                        (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10),
                                        (5, 6), (5, 7), (5, 8), (5, 9), (5, 10),
                                        (6, 7), (6, 8), (6, 9), (6, 10),
                                        (7, 8), (7, 9), (7, 10),
                                        (8, 9), (8, 10),
                                        (9, 10)]], cor.pairs_atoms_threshold_percent_selection(1, 1), "test select all")

    def test_pairs_atoms_threshold_percent_abs_selection(self):
        cor = cs.CorrelationMatrix("data/matrix_10x10_symmetrical.dat", threshold_upper=.5, dist_min=1)
        np.testing.assert_array_equal([[(4, 8)], []], cor.pairs_atoms_threshold_percent_selection(.01, .01),
                                      "test select pairs ok")

        np.testing.assert_array_equal([[], []], cor.pairs_atoms_threshold_percent_abs_selection(0, 0),
                                      "test select nothing")
        np.testing.assert_array_equal([[(1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (1, 8), (1, 9), (1, 10),
                                        (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (2, 10),
                                        (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10),
                                        (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10),
                                        (5, 6), (5, 7), (5, 8), (5, 9), (5, 10),
                                        (6, 7), (6, 8), (6, 9), (6, 10),
                                        (7, 8), (7, 9), (7, 10),
                                        (8, 9), (8, 10),
                                        (9, 10)],
                                       [(1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (1, 8), (1, 9), (1, 10),
                                        (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (2, 10),
                                        (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10),
                                        (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10),
                                        (5, 6), (5, 7), (5, 8), (5, 9), (5, 10),
                                        (6, 7), (6, 8), (6, 9), (6, 10),
                                        (7, 8), (7, 9), (7, 10),
                                        (8, 9), (8, 10),
                                        (9, 10)]], cor.pairs_atoms_threshold_percent_abs_selection(1, 1),
                                      "test select all")

    def test_write_correlation_matrix_file(self):
        return 0

    def test_plot_correlation(self):
        cor = cs.CorrelationMatrix("data/1e15_correlation_matrix.dat")
        cor.plot_correlation_matrix()

        cor = cs.CorrelationMatrix("data/1e15_A_CA_correlation_matrix.dat")
        cor.plot_correlation_matrix()


if __name__ == '__main__':
    unittest.main()
