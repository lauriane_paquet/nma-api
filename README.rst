=====================
NMA - based on WEBNM@
=====================

:Author: Lauriane Paquet
:Version: 1.0

.. contents::

Quick Installation
==================
- install miniconda (Python 3.6) [``https://conda.io/miniconda.html``, ``https://conda.io/docs/user-guide/install``]
- run ``$ ./setup.sh`` of the ``webnma_install`` folder

Run
===
From shell:

- run ``./nma.sh [-p protein_path] [-c correlation_path] [-f fluctuation_path] [-d minimal_distances] [-t threshold] [--tt threshold_type] [--chains chain(s)_used] [--atoms atoms_used] [-a]``.

From Python:

- run ``source activate nma-api-env`` to be able to use it
- run ``python src/main.py [options]``
- when finished, write ``source deactivate`` in terminal

Features
========
- correlation matrix : plot the correlation matrix (in .png and .pdf)
- correlation stick : .pml file (for Pymol) with sticks linking the different correlated atoms within the wanted threshold from the infos of a pdb file and its correlation matrix file.
- fluctuations plot : plot the fluctuations (in .png and .pdf)
- fluctuations plot with II struct : plot the fluctuations with 2nd structures (in .png and .pdf)