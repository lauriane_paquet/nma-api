#!/usr/bin/env python3

import logging
import re
import matplotlib.pyplot as plt
import numpy as np


class Modes(object):
    """
    Class to def Modes.

    Its attributes :
    modes_file = path of the data file
    eigenvalues = eigenvalues
    modes = eigenvectors
    aa = aminoacids concerned
    """
    def __init__(self, modes_file=""):
        self.modes_file = modes_file
        self.eigenvalues = None
        self.modes = {}
        self.aa = []

    def read_modes_webnma_file(self, plot_eigen=False):
        """
        Function to read modes file and stock it's values.

        In : take info of its Mode object plus if a plot of eigenvalues is needed.
        Do : save modes info in Mode.
        Out : nothing.
        """
        modes_lines = False
        name_mode = "mode_"  # + number

        with open(self.modes_file, 'r') as fin:
            while not modes_lines:
                line = fin.readline()

                # line containing all eigenvalues (omega_sq)
                if re.match("^(-?[0-9]+.[0-9]+ )+(-?[0-9]+.[0-9]+)$", line):
                    omega_sq = [float(i) for i in line.split(" ")]
                    self.eigenvalues = self.omega_sq_to_lambda(omega_sq)

                    # init each mode
                    for i in range(len(self.eigenvalues)):
                        self.modes[name_mode + str(i + 1)] = {}

                    if plot_eigen:
                        self.plot_eigenvalues(self.eigenvalues)

                # line containing all amino acids used
                elif re.match("^([A-Z].[A-Z][a-z]{2}[0-9]+ )+([A-Z].[A-Z][a-z]{2}[0-9]+)$", line):
                    aa = line.split(" ")
                    aa[-1] = aa[-1].strip("\n")
                    for i in aa:
                        if i not in self.aa:
                            self.aa.append(i)
                        else:
                            self.aa.append("X." + i)
                            logging.debug("Doublon")
                    modes_lines = True

                    # in each mode init each residue
                    for residue in self.aa:
                        for mode in self.modes.keys():
                            self.modes[mode][residue] = []

            # lines containing modes values
            num_line = 0
            for line in fin:
                if len(line.split()) == len(self.eigenvalues):
                    for i, value in enumerate(line.split()):
                        tmp_name = name_mode + str(i + 1)
                        if int(num_line / 3) < len(self.aa):
                            self.modes[tmp_name][self.aa[int(num_line / 3)]].append(float(value))
                        else:
                            self.modes = []
                            logging.debug("Error: number values per residues not equal to number of residues used")
                            return 1

                else:
                    self.modes = []
                    logging.debug("Error: number values not equal to number of modes")
                    return 1

                num_line += 1

            if num_line != len(self.aa) * 3:
                self.modes = []
                logging.debug("Error: number values per residues not equal to number of residues used")
                return 1

        return 0

    def calc_correlation(self):
        """
        Function to calculate correlation matrix. NOT OPERATIONAL. Careful of weighted vs non weighted calculations.

        In : take only info of its Mode object.
        Do : calculate correlation.
        Out : correlations.
        """
        correlations = []

        for mode_name in self.modes:
            actual_mode = np.array(self.modes[mode_name])

        return correlations

    def calc_fluctuation(self):
        """
        Function to fluctuations. NOT OPERATIONAL. Careful of weighted vs non weighted calculations.

        In : take only info of its Mode object.
        Do : calculate fluctuations.
        Out : fluctuations.
        """
        firsts_modes = ["mode_" + str(i) for i in range(7)]
        s = 0
        for mode_name in self.modes.keys():
            fluct = [0 for i in self.aa]
            eigenvalue_mode_i = self.eigenvalues[int(mode_name.split("_")[-1]) - 1]

            if mode_name not in firsts_modes:
                for i, res_name in enumerate(self.aa):
                    x = self.modes[mode_name][res_name]
                    fluct[i] += ((x[0] ** 2) + (x[1] ** 2) + (x[2] ** 2)) / eigenvalue_mode_i ** 2

        # Calculate sum for normalization
        s = sum([x * x for x in fluct])
        # Normalize the squared fluctuations
        normalized_fluct = [(100 * x * x / s) for x in fluct]

        return normalized_fluct

    def write_modes_files(self):
        """
        Function to write homogenised mode_file

        In : take only info of its Mode object.
        Do : write mode file.
        Out : Nothing.
        """
        return 0

    def plot_eigenvalues(self, eigenvalues):
        """
        Function to plot eigenvalues.

        In : take info of its Mode object.
        Do : write eigenvalues plots into file.
        Out : nothing.
        """
        eigen_file_name = self.modes_file.split('/')[-1].split('.')[:-1][0]

        plt.bar(range(7, len(eigenvalues)), eigenvalues[7:], width=0.08, edgecolor='None', color='k', align='center')
        # hiding y values
        plt.yticks([])
        plt.title("Eigenvalues of {} per mode".format(eigen_file_name))
        plt.xlabel("Modes index")
        plt.ylabel("EigenValues")

        eigen_file_name = "results/" + eigen_file_name
        plt.savefig(eigen_file_name + "_eigenvalues.pdf", format='pdf')
        plt.savefig(eigen_file_name + "_eigenvalues.png", format='png')
        plt.gcf().clear()

    @staticmethod
    def omega_sq_to_lambda(omega_sq=[]):
        """
        Function to switch omega squared values to lambda
        """
        return [(np.sqrt(x) / (2 * np.pi)) for x in omega_sq]


def main():
    return 0


if __name__ == '__main__':
    main()
