#!/usr/bin/env python3

import logging
import numpy as np
import matplotlib.pyplot as plt


class CorrelationMatrix(object):
    """
    Class to define sticks of correlation.

    Its attributes :
    correlation_mat_file = path of the data file
    threshold_upper = between -1 and 1; correlations with a higher value will be kept
    threshold_lower = between -1 and 1; correlations with a lower value will be kept
    corr_matrix = correlation matrix itself
    atoms_correlated = num of atoms concerned (just in case they are not following each other)
    """

    def __init__(self, correlation_mat_file="", threshold_upper=0.9, threshold_lower=-0.9, dist_min=0):
        self.correlation_mat_file = correlation_mat_file
        self.threshold_upper = float(threshold_upper)
        self.threshold_lower = float(threshold_lower)
        self.dist_min = float(dist_min)
        self.corr_matrix = []
        self.atoms_correlated = []

        if correlation_mat_file != "":
            self.read_correlation_matrix_file()

    def read_correlation_matrix_file(self):
        """
        Function to read correlation matrix file and stock it.

        In : take only info of its CorrelationMatrix object.
        Do : save correlation matrix as a numpy object in corrMatrix.
        Out : nothing.
        """

        logging.debug("Reading correlation matrix file.")
        array_mat = []
        i = -1
        first_line = True

        with open(self.correlation_mat_file, 'r') as fin:
            for line in fin:
                # skip empty lines
                if len(line.strip()) > 0:
                    # first line
                    if first_line:
                        self.atoms_correlated = [int(i) for i in line.split()]
                        i = 0
                        first_line = False

                        if not np.size(self.atoms_correlated) > 0:
                            logging.debug("Error : in file given : first line not correct")
                            return 1
                    # others lines
                    else:
                        tmp = [float(i) for i in line.split()[1:]]
                        if (int(line.split()[0]) == self.atoms_correlated[i]) and \
                                (len(tmp) == np.size(self.atoms_correlated)):
                            i += 1
                            array_mat.append(tmp)
                        else:
                            logging.debug("Error : in file given : numbers of atoms aren't identical or matrix " +
                                          "isn't a square.")
                            return 1
        if not array_mat:
            logging.debug("Error : in file given : is your file empty ?")
            return 1

        self.corr_matrix = np.array(array_mat)
        if np.shape(self.corr_matrix)[0] != np.shape(self.corr_matrix)[1] or self.test_matrix_symmetry() != 0:
            self.corr_matrix = []
            logging.debug("Error : in file given : matrix is not symmetrical or square")
            return 1
        return 0

    def test_matrix_symmetry(self):
        """
        Function to see if matrix is really symmetrical.

        In : take only info of its CorrelationMatrix object.
        Do : test
        Out : error or not
        """
        for i in range(0, len(self.atoms_correlated)):
            for j in range(i + 1, len(self.atoms_correlated)):
                if self.corr_matrix[i, j] != self.corr_matrix[j, i]:
                    logging.error("Error : matrix given is not symmetrical")
                    return 1
        return 0

    def change_threshold(self, new_threshold_upper, new_threshold_lower):
        """
        Function to change thresholds. (inf & -inf means user doesn't want any value)

        In : take new values.
        Do : change values in object
        Out : nothing
        """
        if (-1 <= new_threshold_upper <= 1 or new_threshold_upper == float('Inf')) and \
           (-1 <= new_threshold_lower <= 1 or new_threshold_lower == -float('Inf')):
            self.threshold_lower = new_threshold_lower
            self.threshold_upper = new_threshold_upper
        else:
            logging.debug("Error : new threshold values must be between -1 and 1 or Inf (values given = [" +
                          str(new_threshold_upper) + ", " + str(new_threshold_lower) + "])")

    def change_dist_min(self, new_dist_min):
        """
        Function to change distance (in Å) min between pairs of atoms.

        In : take new value.
        Do : change value in object
        Out : nothing
        """
        if new_dist_min >= 0:
            self.dist_min = new_dist_min
        else:
            logging.debug("Error : new distance min value must be > 0 (value given = " + str(new_dist_min) + ")")
            return 1

    def pairs_atoms_threshold_selection(self):
        """
        Function to select desired atoms and stock them.

        In : take only info of its CorrelationMatrix object.
        Do : save real id of pairs of atoms of threshold
        Out : pairs of atoms
        """
        pairs_correlated = []
        pairs_anti_correlated = []
        for i in range(0, len(self.atoms_correlated)):
            for j in range(i + 1, len(self.atoms_correlated)):
                tmp = self.corr_matrix[i, j]
                if tmp != float('Inf'):
                    if tmp >= self.threshold_upper:
                        pairs_correlated.append(tuple([self.atoms_correlated[i], self.atoms_correlated[j]]))
                    if tmp <= self.threshold_lower:
                        pairs_anti_correlated.append(tuple([self.atoms_correlated[i], self.atoms_correlated[j]]))

        return tuple([pairs_correlated, pairs_anti_correlated])

    def pairs_atoms_threshold_percent_selection(self, percent_threshold_upper=.1, percent_threshold_lower=.1):
        """
        Function to select desired atoms with a % threshold and stock them.

        In : take info of its CorrelationMatrix object + percent_threshold ([upper_in_%,lower_in_%];0<=values<=1).
        Do : select correct threshold value and run pairs_atoms_threshold_selection accordingly.
        Out : pairs of atoms
        """
        if not 0 <= percent_threshold_upper <= 1 and 0 <= percent_threshold_lower <= 1:
            logging.debug("Error : percents threshold values must be between 0 and 1 (values given = [" +
                          str(percent_threshold_upper) + ", " + str(percent_threshold_lower) + "])")
            return -1
        if (self.corr_matrix != float('Inf')).sum() == 0:
            self.change_threshold(float('Inf'), -float('Inf'))
            return 0

        if not percent_threshold_upper == 0:
            percent_threshold_upper = np.percentile(np.ma.masked_values(self.corr_matrix, float('Inf')).compressed(),
                                                    int((1 - percent_threshold_upper) * 100))
        else:
            percent_threshold_upper = float('Inf')

        if not percent_threshold_lower == 0:
            percent_threshold_lower = np.percentile(np.ma.masked_values(self.corr_matrix, float('Inf')).compressed(),
                                                    int(percent_threshold_lower * 100))
        else:
            percent_threshold_lower = -float('Inf')
        self.change_threshold(percent_threshold_upper, percent_threshold_lower)
        return self.pairs_atoms_threshold_selection()

    def pairs_atoms_threshold_percent_abs_selection(self, percent_threshold_upper=.1, percent_threshold_lower=.1):
        """
        Function to select desired atoms with a % threshold when matrix abs and stock them.

        In : take info of its CorrelationMatrix object + percent_threshold ([upper_in_%,lower_in_%];0<=values<=1).
        Do : select correct threshold value and run pairs_atoms_threshold_selection accordingly.
        Out : pairs of atoms
        """
        if not 0 <= percent_threshold_upper <= 1 and 0 <= percent_threshold_lower <= 1:
            logging.debug("Error : percents threshold values must be between 0 and 1/2 (values given = [" +
                          str(percent_threshold_upper) + ", " + str(percent_threshold_lower) + "])")
            return -1
        if (self.corr_matrix != float('Inf')).sum() == 0:
            self.change_threshold(float('Inf'), -float('Inf'))
            return 0

        if percent_threshold_upper == 0:
            percent_threshold_upper = float('Inf')
        elif percent_threshold_upper == 1:
            percent_threshold_upper = np.percentile(np.ma.masked_values(self.corr_matrix, float('Inf')).compressed(),
                                                                              int((1 - percent_threshold_upper) * 100))
        else:
            percent_threshold_upper = np.percentile(np.ma.masked_values(abs(self.corr_matrix),
                                                                        float('Inf')).compressed(),
                                                    int((1 - percent_threshold_upper) * 100))
        if percent_threshold_lower == 0:
            percent_threshold_lower = -float('Inf')
        elif percent_threshold_lower == 1:
            percent_threshold_lower = np.percentile(np.ma.masked_values(self.corr_matrix, float('Inf')).compressed(),
                                                    int(percent_threshold_lower * 100))
        else:
            if not percent_threshold_upper == float('Inf'):
                percent_threshold_lower = -percent_threshold_upper
            else:
                percent_threshold_lower = -np.percentile(np.ma.masked_values(abs(self.corr_matrix),
                                                                             float('Inf')).compressed(),
                                                         int((1 - percent_threshold_lower) * 100))

        self.change_threshold(percent_threshold_upper, percent_threshold_lower)
        return self.pairs_atoms_threshold_selection()

    def write_correlation_matrix_file(self):
        """
        Function to write correlation matrix file.

        In : take only info of its CorrelationMatrix object.
        Do : write correlation matrix into file.
        Out : nothing.
        """
        logging.debug("Writing correlation_matrix file.")

        with open(self.correlation_mat_file, 'w') as fout:
            # first line
            for i in self.atoms_correlated:
                fout.write(str(self.atoms_correlated[i]) + " ")
            fout.write("\n")
            for i, line in enumerate(self.corr_matrix):
                fout.write(str(self.atoms_correlated[i]) + " ")
                for j in line:
                    fout.write(str(j) + " ")
                fout.write("\n")

    def plot_correlation_matrix(self):
        """
        Function to plot correlation matrix. Will add a xmin/xmax/other if needs occur.

        In : take only info of its CorrelationMatrix object.
        Do : write correlation plots into file.
        Out : nothing.
        """
        cor_file_name = self.correlation_mat_file.split('/')[-1].split('.')[:-1][0]

        plt.contourf(range(len(self.atoms_correlated)),
                     range(len(self.atoms_correlated)),
                     self.corr_matrix,
                     10,
                     cmap=plt.cm.bwr,  # RdBu ou bwr
                     vmax=+1,
                     vmin=-1)
        plt.colorbar()
        plt.title("Correlation Matrix of {}".format(cor_file_name))
        plt.xlabel("Residue index")
        plt.ylabel("Residue index")

        cor_file_name = "results/" + cor_file_name
        plt.savefig(cor_file_name + ".pdf")
        plt.savefig(cor_file_name + ".png")
        plt.gcf().clear()


def main():
    return 0


if __name__ == '__main__':
    main()
