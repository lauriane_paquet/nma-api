#!/usr/bin/env python3

import logging
import os
from Bio.PDB import *
import correlation_matrix as cs
import fluctuation as fl
import modes as md
import numpy as np
import subprocess
import re


class Molecule(object):
    """
    Class to def molecules.

    Its attributes :
    protein = BioPython object (all infos from PDB file)
    correlation_matrix = CorrelationMatrix object (all infos from correlation file)
    """

    def __init__(self):
        self.data_path = None
        self.pdb_type = None
        self.pdb_protein = None
        self.correlation = None
        self.fluctuation = None
        self.mode = None

    def delete_hetatm(self, pdb_file):
        """
        Function to remove HETATM (water molecules) lines from pdb file.
        In : pdb file with HETATM lines needing to be removed.
        Do : create a new pdb file without any HETATM lines
        Out : new cleaned file
        """
        new_pdb_file = "/".join(pdb_file.split("/")[:-1]) + "/" + \
                       pdb_file.split("/")[-1].split(".")[0] + "_clean." + self.pdb_type
        command = "grep -v 'HETATM' {} > {}".format(pdb_file, new_pdb_file)
        os.system(command)
        return new_pdb_file

    def init_protein(self, pdb_file):
        """
        Function to init protein from pdb file.
        In : pdb file to read and extract infos from
        Do : clean pdb file and stock new BioPython object with cleaned pdb in Molecule object
        Out : nothing
        """
        if pdb_file != "":
            self.data_path = "/".join(pdb_file.split("/")[:-1])
            self.pdb_type = pdb_file.split(".")[-1]

            if self.pdb_type == "pdb":
                parser = PDBParser(PERMISSIVE=1)  # permissive; allow reading buggy pdb files
            elif self.pdb_type == "cif":
                parser = MMCIFParser()
            else:
                logging.error("Error : wrong protein format (file given : " + pdb_file + ")")
                return 1
            pdb_file = self.delete_hetatm(pdb_file)
            self.pdb_protein = parser.get_structure(pdb_file.split('/')[-1].split('.')[0], pdb_file)
            os.system("rm " + pdb_file)
            if not Selection.unfold_entities(self.pdb_protein, target_level='A') == []:
                return 0
        logging.debug("Error : need a pdb file not empty (file given : " + pdb_file + ")")
        return 1

    def init_correlation(self, correlation_matrix_file=None, threshold_upper=0.9, threshold_lower=-0.9):
        """
        Function to init correlation matrices from .dat file.
        In : take name file + threshold_upper and threshold_lower.
        Do : save correlation object in protein object.
        Out : nothing.
        """
        if not correlation_matrix_file:
            if self.data_path:
                correlation_matrix_file = self.data_path + "/" + \
                                          str("_".join(self.pdb_protein.get_id().split("_")[:-1])) + \
                                          "_correlation_matrix.dat"
            else:
                logging.debug("Error : no correlation matrix file given")
                return 1
        if os.path.isfile(correlation_matrix_file):
            if not self.data_path:
                self.data_path = "/".join(correlation_matrix_file.split("/")[:-1])
            self.correlation = cs.CorrelationMatrix(correlation_matrix_file, threshold_upper, threshold_lower)
        else:
            logging.debug("Error : correlation matrix file doesn't exist")
            return 1
        return 0

    def init_fluctuation(self, fluctuation_file=None):
        """
        Function to init fluctuations from .dat file.
        In : take name file
        Do : save fluctuation object in protein object.
        Out : nothing.
        """
        if not fluctuation_file:
            if self.data_path:
                fluctuation_file = self.data_path + "/" + str("_".join(self.pdb_protein.get_id().split("_")[:-1])) + \
                                  "__fluctuationsplot.dat"
            else:
                logging.debug("Error : no fluctuation file given")
                return 1
        if os.path.isfile(fluctuation_file):
            if not self.data_path:
                self.data_path = "/".join(fluctuation_file.split("/")[:-1])
            self.fluctuation = fl.Fluctuation(fluctuation_file)
        else:
            logging.debug("Error : Fluctuation file doesn't exist")
            return 1
        return 0

    def init_modes(self, modes_file=None, mode_type=None):
        """
        Function to init Modes.
        In : take name file
        Do : save fluctuation object in protein object.
        Out : nothing.
        """
        if os.path.isfile(modes_file):
            if not self.data_path:
                self.data_path = "/".join(modes_file.split("/")[:-1])
            self.mode = md.Modes(modes_file)
        else:
            logging.debug("Error : Mode file doesn't exist")
            return 1

        if mode_type == "webnma":
            self.mode.read_modes_webnma_file(True)
        return 0

    def get_atoms_serial_number(self, chains=[], atom_type='CA'):
        """
        Function to get serial_number of all atoms of one type (CA, O, None, ...) and some chains.
        In : type of atom to be filtered (if needed)
        Do : select atoms
        Out : atoms of wanted type
        """
        selection = Selection.unfold_entities(self.pdb_protein, target_level='A')
        list_atoms = []
        if len(chains) > 0:
            if atom_type is not None:
                for chain in chains:
                    list_atoms.append([atom.get_serial_number() for atom in selection if atom.get_full_id()[2] == chain
                                      and atom.get_id() == atom_type])
            else:
                for chain in chains:
                    list_atoms.append([atom.get_serial_number() for atom in selection if atom.get_full_id()[2] == chain])
            list_atoms = [num_atom for sublist in list_atoms for num_atom in sublist]
        else:
            if atom_type is None:
                list_atoms = [atom.get_serial_number() for atom in selection]
            else:
                list_atoms = [atom.get_serial_number() for atom in selection if atom.get_id() == atom_type]
        return list_atoms

    def hide_close_pairs_atoms(self, dist_min=0, chains_used=[], atoms_used='CA'):
        """
        Function to hide pairs of atoms with a distance < dist_min.

        In : take info of its CorrelationMatrix object, distance minimum between 2 atoms and atoms used
        Do : change value of pairs concerned to +Inf
        Out : nothing
        """
        self.correlation.change_dist_min(dist_min)

        prot = Selection.unfold_entities(self.pdb_protein, target_level='A')
        serial_numbers = [atom.serial_number for atom in prot]
        prot_dict = dict(zip(serial_numbers, prot))
        atom_ser_num = self.get_atoms_serial_number(chains_used, atoms_used)

        if self.correlation.dist_min > 0:
            for i in range(0, len(self.correlation.atoms_correlated)):
                for j in range(i + 1, len(self.correlation.atoms_correlated)):
                    if prot_dict[atom_ser_num[j]] - prot_dict[atom_ser_num[i]] < self.correlation.dist_min:
                        self.correlation.corr_matrix[i, j] = self.correlation.corr_matrix[j, i] = float('Inf')
        for i in range(0, len(self.correlation.atoms_correlated)):
            self.correlation.corr_matrix[i, i] = float('Inf')

    def get_pairs_atoms_infos(self, chains_used=[], atoms_used='CA', selection=2, threshold_upper=.1, threshold_lower=.1):
        """
        Function to get desired atoms with either :
            - a threshold (between -1 and 1 for min and max),
            - a % threshold (between 0 and 1 for min and max),
            - an abs(%) threshold (between 0 and 1 for min and max),
            and return them in a list depending of their chain and correlation type
            (an atom in A chain anti-correlated to an atom in B chain will be in the list[0].index('1AB').
        In : take info of its :
                - Molecule object,
                - chain_used (A, B, C, .... or a mix)
                - atoms_used
                - type of selection desired (0 = hard threshold, 1 = % and 2 = abs(%)),
                - threshold_upper
                - threshold_lower
        Do : select right atoms and stock them
        Out : return atoms
        """
        # select atoms concerned
        atoms_selected = None
        if selection == 0:
            self.correlation.change_threshold(threshold_upper, threshold_lower)
            atoms_selected = self.correlation.pairs_atoms_threshold_selection()
        elif selection == 1:
            atoms_selected = self.correlation.pairs_atoms_threshold_percent_selection(
                threshold_upper, threshold_lower)
        elif selection == 2:
            atoms_selected = self.correlation.pairs_atoms_threshold_percent_abs_selection(
                threshold_upper, threshold_lower)
        else:
            logging.debug("Error : selection need to be an int between 0 and 2 (value given = " + str(selection) + ")")

        # to allow to find atoms'infos
        atom_ser_num = self.get_atoms_serial_number(chains_used, atoms_used)
        prot = Selection.unfold_entities(self.pdb_protein, target_level='A')
        serial_numbers = [atom.serial_number for atom in prot]
        prot_dict = dict(zip(serial_numbers, prot))

        # selection of info and stock it
        list_pairs_atoms = [[]]
        if isinstance(atoms_selected, (list, tuple, np.ndarray)):
            for i_color, correlation_type in enumerate(atoms_selected):
                for i_pair, pair_atoms in enumerate(correlation_type):
                    full_id = []
                    # get infos from pair
                    for num_atom in pair_atoms:
                        full_id.append(prot_dict[atom_ser_num[num_atom - 1]].get_full_id())

                    # put pairs at right place
                    obj_name = [str(i_color) + full_id[0][2] + full_id[1][2],
                                str(i_color) + full_id[1][2] + full_id[0][2]]

                    if obj_name[0] in list_pairs_atoms[0]:
                        list_pairs_atoms[list_pairs_atoms[0].index(obj_name[0]) + 1].append(
                            [full_id[0][3][1], full_id[1][3][1]])

                    elif obj_name[1] in list_pairs_atoms[0]:
                        list_pairs_atoms[list_pairs_atoms[0].index(obj_name[1]) + 1].append(
                            [full_id[1][3][1], full_id[0][3][1]])

                    else:
                        list_pairs_atoms[0].append(obj_name[0])
                        list_pairs_atoms.append([[full_id[0][3][1], full_id[1][3][1]]])
        return list_pairs_atoms

    def write_correlation_sticks_pml(self, atoms_selected, atoms_used='CA'):
        """
        Function to write a pml file (for Pymol) showing sticks for correlated and anti-correlated atoms
        In : take info of its Molecule object, atoms_selected to be written, atoms_used for correlation calculations
        Do : write pml file with colored sticks
        Out : Nothing
        """
        colors = ['red', 'blue']

        # files and names
        pdb_name = "_".join(self.pdb_protein.get_id().split("_")[:-1])
        thresholds = str(self.correlation.threshold_upper) + "_" + str(self.correlation.threshold_lower)

        fout_name = "/".join(["results/", pdb_name]) + "_corr_dist_" + str(self.correlation.dist_min) + \
                    "_lim_" + thresholds + ".pml"

        # writing pml file
        with open(fout_name, "w") as fout:
            fout.write("# Pymol script for visualising correlation networks on input protein from nma_api\n"
                       "# To visualise, load the original input PDB file in Pymol,\n"
                       "# then run this script file within the same session.\n"
                       "# Objects are created where pairs of residues that have a correlation value within a\n"
                       "# threshold and a minimum distance of {}Å, are represented by sticks,"
                       .format(str(self.correlation.dist_min))
                       + "forming uninterrupted networks\n"
                       "# if they cover a larger region within the matrix.\n"
                       "# Red sticks represent positive correlations (value > {})\n"
                       .format(str(self.correlation.threshold_upper))
                       + "# Blue sticks (if present) represent negative correlations (value < {})\n"
                       .format(str(self.correlation.threshold_lower))
                       + "\n\n")

            fout.write("load %s/%s.%s\n" % (os.path.abspath(self.data_path), pdb_name, self.pdb_type))
            fout.write("hide everything\n")
            fout.write("set cartoon_trace, 1\n")
            fout.write("show cartoon, name %s\n\n" % (atoms_used.upper()))

            if isinstance(atoms_selected, (list, tuple, np.ndarray)):
                for i_gp, group in enumerate(atoms_selected[1:]):
                    # write create object
                    list_atoms = []
                    for pair in group:
                        for i, atom in enumerate(pair):
                            res = "(resi " + str(pair[i]) + " and chain " + atoms_selected[0][i_gp][i + 1] + ")"
                            if res not in list_atoms:
                                list_atoms.append(res)
                    list_atoms = ", ".join(list_atoms)
                    dist = "{:.1f}".format(self.correlation.dist_min)
                    if i_gp == 0:
                        lim = "{:.3f}".format(self.correlation.threshold_upper)
                    else:
                        lim = "{:.3f}".format(self.correlation.threshold_lower)
                    group_name = atoms_selected[0][i_gp] + "_" + dist + "_" + lim + "_" + pdb_name
                    fout.write("create %s, %s and name %s and (%s)\n"
                               % (group_name, pdb_name, atoms_used, list_atoms))

                    # write create bonds
                    for pair in group:
                        res = []
                        for i, atom in enumerate(pair):
                            res.append("resi " + str(atom) + " and chain " + atoms_selected[0][i_gp][i + 1] + "")
                        fout.write("bond (%s and %s), (%s and %s)\n"
                                   % (group_name, res[0], group_name, res[1]))

                    # write how bonds should be showed
                    fout.write("show sticks, %s\n" % group_name)
                    fout.write("set stick_radius, 0.2, %s\n" % group_name)
                    fout.write("color %s, %s\n" % (colors[int(atoms_selected[0][i_gp][0])], group_name))
                    fout.write("hide cartoon, %s\n\n" % group_name)

            fout.write("zoom\n")

    def read_dssp_file(self, dssp_result_path):
        """
        Function to read dssp file and return infos
        In: path of dssp result
        Do: get the results and store them
        Out: Structures
        """
        # read DSSP results and stock infos
        # G = 3-turn helix. Min length = 3 res.
        # H = 4-turn helix. Min length = 4 res.
        # I = 5-turn helix. Min length = 5 res.
        # T = hydrogen bonded turn (3, 4 or 5 turn).
        # E = β-sheet. Min length = 2 res.
        # B = single pair β-sheet hydrogen bond formation.
        # S = bend (the only non-hydrogen-bond based assignment).
        # C = coil.
        protein_line = False
        types_structure = ['G', 'H', 'I', 'T', 'E', 'B', 'S', 'C']
        structures = []
        chg_chain = 0
        for i in types_structure:
            structures.append([i])

        with open(dssp_result_path, 'r') as fin:
            last_struct = ""
            for line in fin:
                # if we are in the zone of interest of the dssp file
                if protein_line:
                    if '*' in line:
                        chg_chain += 1
                        last_struct = ""

                    else:
                        line = line.split()
                        # if the line contain a secondary structure
                        if line[4] in types_structure:
                            # if the structure is the same as last one, add it to this zone
                            if last_struct == line[4]:
                                i_struct = types_structure.index(line[4])
                                structures[i_struct][len(structures[i_struct]) - 1][1] = int(line[0]) - chg_chain

                            # if the structure is a different type, create new zone
                            else:
                                new_zone = int(line[0]) - chg_chain
                                structures[types_structure.index(line[4])].append([new_zone, new_zone])
                                last_struct = line[4]
                        # if the line don't have a secondary structure
                        else:
                            last_struct = ""

                elif re.match("^ *# *RESIDUE *AA * STRUCTURE *.*", line):
                    protein_line = True

        if sum(len(x) for x in structures) > 0:
            return structures
        else:
            logging.debug("No secondary structure found")
            return None

    def calculate_secondary_structures(self, dssp_result_path):
        """
        Function to calculate secondary structures for plots.
        In : take info of its Molecule object
        Do : save secondary structure file
        Out : Nothing
        """
        file_path = os.path.abspath("results/") + "tmp.pdb"

        # save prot into a readable (temporary) PDB file for DSSP
        io = PDBIO()
        io.set_structure(self.pdb_protein)
        io.save(file_path)

        # calculate and save DSSP results into a file
        filout_dssp = open(dssp_result_path, "w")
        p = subprocess.run("mkdssp -i " + file_path,
                           shell=True,
                           stdout=filout_dssp,
                           stderr=subprocess.PIPE)
        filout_dssp.close()
        os.system("rm " + file_path)

        dssp_error = p.stderr.decode('utf-8')
        if dssp_error:
            logging.error("Error: " + dssp_error)
            return dssp_error
        else:
            return 0


def main():
    return 0


if __name__ == '__main__':
    main()
