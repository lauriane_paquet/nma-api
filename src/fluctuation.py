#!/usr/bin/env python3

import logging
import matplotlib.pyplot as plt


class Fluctuation(object):
    """
    Class to def Fluctuations.

    Its attributes :
    fluctuation_file = path of the data file
    fluctuations = fluctuations values
    num_atoms = num of atoms concerned
    """

    def __init__(self, fluctuation_file=""):
        self.fluctuation_file = fluctuation_file
        self.fluctuations = []
        self.num_atoms = []

        if fluctuation_file != "":
            self.read_fluctuation_file()

    def read_fluctuation_file(self):
        """
        Function to read fluctuation file and stock it's values.

        In : take only info of its Fluctuation object.
        Do : save fluctuations in list in Fluctuation.
        Out : nothing.
        """
        logging.debug("Reading fluctuation file.")
        with open(self.fluctuation_file, 'r') as fin:
            for line in fin:
                # skip empty lines
                if len(line.strip()) > 0:
                    self.num_atoms.append(int(line.split()[0]))
                    self.fluctuations.append(float(line.split()[1]))
        if not self.fluctuations or len(self.num_atoms) != len(self.fluctuations):
            logging.debug("Error : in file given : is your file empty or are some values missing ?")
            return 1
        return 0

    def write_fluctuation_file(self):
        """
        Function to write fluctuation file.

        In : take only info of its Fluctuation object.
        Do : write fluctuations of lists in Fluctuation to file.
        Out : nothing.
        """
        logging.debug("Writing fluctuation file.")

        with open("results/" + self.fluctuation_file.split('/')[-1], 'w') as fout:
            for i in range(len(self.num_atoms)):
                fout.write("{}\t{}\n".format(self.num_atoms[i], self.fluctuations[i]))

    def plot_fluctuations(self, color='black', line_width=0.7):
        """
        Function to plot fluctuations. Will add a xmin/xmax/other if needs occur.

        In : take only info of its Fluctuation object.
        Do : write fluctuations plots into file.
        Out : nothing.
        """
        flu_file_name = self.fluctuation_file.split('/')[-1].split('.')[:-1][0]
        plt.plot(range(len(self.num_atoms)), self.fluctuations, color, lw=line_width)
        plt.title("Normalised fluctuation of {}".format(flu_file_name))
        plt.xlabel("Residue index")
        plt.ylabel("Fluctuation")

        flu_file_name = "results/" + flu_file_name
        plt.savefig(flu_file_name + ".pdf")
        plt.savefig(flu_file_name + ".png")
        plt.gcf().clear()

    def plot_fluctuations_and_structure(self, structures=[], struct_plotted=['G', 'H', 'I', 'E'], line_width=0.5):
        """
        Function to plot fluctuations and it's secondary structure. Will add a xmin/xmax/other if needs occur.

        In : take info of its Fluctuation object and structures.
        Do : write fluctuations plots into file.
        Out : nothing.
        """
        flu_file_name = self.fluctuation_file.split('/')[-1].split('.')[:-1][0]
        # colors of structures to plot, in same order as in molecule
        # color = ['Blue', 'Cyan', 'DarkBlue', 'DarkGreen', 'Red', 'DarkRed', 'Green', 'Grey']
        # legend = ["G = 3-turn helix (min : 3 res).",
        #           "H = 4-turn helix (min : 4 res)",
        #           "I = 5-turn helix (min : 5 res)",
        #           "T = hydrogen bonded turn (3, 4 or 5 turn).",
        #           "E = β-sheet        (min : 2 res)",
        #           "B = single pair β-sheet hydrogen bond formation.",
        #           "S = bend (the only non-hydrogen-bond based assignment).",
        #           "C = coil."]

        color = ['red', 'red', 'red', 'Grey', 'darkturquoise', 'darkturquoise', 'Green', 'LightGrey']
        legend = ["Helix",
                  None,
                  None,
                  "Hydrogen bonded turn (3, 4 or 5 turn)",
                  "β-sheet",
                  None,
                  "Bend",
                  "Coil."]

        # get index of structures to plot
        i_struct_to_plot = []
        for i, struct in enumerate(structures):
            if struct[0][0] in struct_plotted:
                i_struct_to_plot.append(i)

        # plot
        plt.plot(range(len(self.num_atoms)), self.fluctuations, color='black', lw=line_width)
        plt.title("Normalised fluctuation of {}".format(flu_file_name))
        plt.xlabel("Residue index")
        plt.ylabel("Fluctuation")
        for i in i_struct_to_plot:
            for zone in structures[i][1:]:
                if zone[1] - zone[0] >= 3:
                    if legend[i]:
                        plt.axvspan(zone[0], zone[1], facecolor=color[i], alpha=0.5, label=legend[i])
                        legend[i] = None
                    else:
                        plt.axvspan(zone[0], zone[1], facecolor=color[i], alpha=0.5)

        plt.legend()
        flu_file_name = "results/" + flu_file_name
        plt.savefig(flu_file_name + "_with_structure_II.pdf", format='pdf')
        plt.savefig(flu_file_name + "_with_structure_II.png", format='png')
        plt.gcf().clear()


def main():
    return 0


if __name__ == '__main__':
    main()
