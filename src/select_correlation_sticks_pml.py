#!/usr/bin/env python3

import logging
import re
import sys


class Pml(object):
    """
    python [script] "[PATH].pml" "resi [X] and chain [Y]"
    """
    def __init__(self, pml_file="", res_name="", type_obj_name=None):
        self.pml_file = pml_file
        self.res_name = res_name.strip()
        self.type_obj_name = type_obj_name
        self.create_infos = None
        self.pairs = []
        self.color_pairs = []
        self.read_pml_select_res()

    def read_pml_select_res(self):
        """
        Function to pml file and stock links to do.

        In : pml object infos
        Do : select pairs wanted
        Out : Nothing
        """
        is_here = False

        logging.debug("Reading file.")
        actual_color = ""
        with open(self.pml_file, 'r') as fin:
            for line in fin:
                # skip empty lines
                if len(line.strip()) > 0:
                    if line.startswith('create'):
                        if self.res_name in line:
                            if not self.type_obj_name or (self.type_obj_name and self.type_obj_name in line):
                                is_here = True
                                actual_color = line.split(" ")[1][0]
                                if not self.create_infos:
                                    tmp = re.split('[,(]', line)
                                    self.create_infos = tmp[1].strip()
                                    if self.type_obj_name:
                                        self.create_infos += self.type_obj_name
                        else:
                            is_here = False

                    if is_here == True and line.startswith('bond'):
                        if self.res_name in line:
                            tmp = re.split('[()]', line)
                            tmp = [[tmp[1].split(" ")[3], tmp[1].split(" ")[6]], [tmp[3].split(" ")[3], tmp[3].split(" ")[6]]]
                            self.pairs.append(tmp)
                            self.color_pairs.append(actual_color)

    def write_obj_pml(self):
        mol_name = self.pml_file.split("/")[-1].split("_")
        i_name = mol_name.index("corr")
        mol_name = "_".join(mol_name[:i_name])

        if self.type_obj_name:
            res_linked = self.type_obj_name + "".join(self.res_name.split(" "))
        else:
            res_linked = "".join(self.res_name.split(" "))
        fout_name = "/".join(self.pml_file.split("/")[:-1]) + "/" + mol_name + "_stk_lk_2_" + res_linked + ".pml"
        
        # writing pml file
        with open(fout_name, "w") as fout:
            fout.write("# Pymol script for visualising correlation sticks linked to an atom\n\n")

            list_atoms = []
            for pair in self.pairs:
                for atom in pair:
                    res = "(resi " + str(atom[0]) + " and chain " + str(atom[1]) + ")"
                    if res not in list_atoms:
                        list_atoms.append(res)
            list_atoms = ", ".join(list_atoms)

            res = self.res_name.split(" ")
            res_name = res[1] + res[-1]
            group_name = ["0_%s" % res_name, "1_%s" % res_name]
            colors = ["red", "blue"]

            for i_gp, gp in enumerate(group_name):
                fout.write("create %s, %s (%s)\n" % (gp, self.create_infos, list_atoms))
                for i, color in enumerate(self.color_pairs):
                    if color == gp.split("_")[0]:
                        res = []
                        for atom in self.pairs[i]:
                            res.append("resi " + str(atom[0]) + " and chain " + str(atom[1]) + "")
                        fout.write("bond (%s and %s), (%s and %s)\n" % (gp, res[0], gp, res[1]))
                fout.write("show sticks, %s\n" % gp)
                fout.write("set stick_radius, 0.2, %s\n" % gp)
                fout.write("color %s, %s\n" % (colors[i_gp], gp))
                fout.write("hide cartoon, %s\n\n" % gp)

         
def main():
    if 2 < len(sys.argv) <= 4:
        if len(sys.argv) <= 3:
            correlations = Pml(sys.argv[1], sys.argv[2], None)
        else:
            correlations = Pml(sys.argv[1], sys.argv[2], sys.argv[3])
        correlations.write_obj_pml()
    else:
        logging.debug("Number of arguments incorrect")


if __name__ == '__main__':
    main()

