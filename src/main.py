#!/usr/bin/env python3

import os
import sys
import argparse
import logging
import molecule as mol


def get_parameters():
    """
    Function to get user parameters.

    In : Nothing. Get arguments from user.
    Do : Process the arguments and store them
    Out : User parameters.
    """
    # arguments dictionary (Default values)
    parameters = {"ProteinFile": None,
                  "CorrelationFile": None,
                  "FluctuationFile": None,
                  "DistMin": 0,
                  "Threshold": [2, 0.5, 0.5],
                  "ChainUsed": [],
                  "AtomsUsed": "CA",
                  "AllAnalysis": False
                  }

    if len(sys.argv) <= 1:
        sys.exit("Error: No argument given (min: 1 file)")

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # parser.add_argument(name or flags...[, action][, nargs][, const][, default][, type][, choices][, required]
    #                     [, help][, metavar][, dest])
    parser.add_argument('-p', '--prot', action='store', nargs=1, help="(X)PDB path", dest='prot_file')
    parser.add_argument('-c', '--corr', action='store', nargs=1, help="correlation file path", dest='corr_file')
    parser.add_argument('-f', '--fluct', action='store', nargs=1, help="fluctuation file path", dest='fluct_file')
    parser.add_argument('-d', '--distMin', action='store', nargs=1, type=float,
                        help="dist min between atoms for correlation sticks", dest='distMin')
    parser.add_argument('-t', '--threshold', action='store', nargs=2, type=float,
                        help="threshold max and min to be used for correlation sticks", dest='threshold')
    parser.add_argument('--tt', '--thresholdType', action='store', nargs=1, type=int, choices=[0, 1, 2],
                        help="threshold type to be used for correlation sticks: "
                             "1: exact value, 2: percentile, 3: abs(percentile)", dest='thresholdType')
    parser.add_argument('--chains', action='store', nargs='+', help="chain(s) used for mode calculation", dest='chains')
    parser.add_argument('--atoms', action='store', nargs=1, help="atom(s) used for mode calculation", dest='atoms')
    parser.add_argument('-a', '--all', action='store_true', help="all analysis possible will be done", dest='all')

    args = parser.parse_args()

    # check if a protein file was given and if it exist
    if args.prot_file is not None:
        if os.path.exists(args.prot_file[0]):
            parameters["ProteinFile"] = os.path.abspath(args.prot_file[0])
            print("Protein file given.")
        else:
            sys.exit("Error: Protein file not found")

    # check if a correlation file was given and if it exist
    if args.corr_file is not None:
        if os.path.exists(args.corr_file[0]):
            parameters["CorrelationFile"] = os.path.abspath(args.corr_file[0])
            print("Correlation matrix file given. Simple analyses on it will be done.")
            # check parameters in case of correlations sticks
            if parameters["ProteinFile"]:
                print("Correlation sticks analysis - parameters:")
                if args.distMin is not None:
                    parameters["DistMin"] = args.distMin[0]
                print("    Minimal distance between 2 atoms: " + str(parameters["DistMin"]))

                if args.threshold is not None:
                    parameters["Threshold"][1] = args.threshold[0]
                    parameters["Threshold"][2] = args.threshold[1]
                    if parameters["Threshold"][1] < 0 or parameters["Threshold"][2] < 0:
                        if args.thresholdType and args.thresholdType[0] not in ["val", 0]:
                                logging.debug("Wrong threshold type given. Changed to absolute value.")
                        parameters["Threshold"][0] = 0
                    elif args.thresholdType:
                        parameters["Threshold"][0] = args.thresholdType[0]

                elif args.thresholdType is not None:
                    parameters["Threshold"][0] = args.thresholdType[0]
                print("    Threshold [type, upper, lower]: " + str(parameters["Threshold"]))

        else:
            sys.exit("Error: Correlation file not found")

    # check if a fluctuation file was given and if it exist
    if args.fluct_file is not None:
        if os.path.exists(args.fluct_file[0]):
            parameters["FluctuationFile"] = os.path.abspath(args.fluct_file[0])
            print("Fluctuation file given. Simple analyses on it will be done.")

            if parameters["ProteinFile"]:
                print("Fluctuation with secondary structures will be plotted.")
        else:
            sys.exit("Error: Fluctuation file file not found")

    # add chains and atoms if needed
    if args.chains is not None:
        for chain in args.chains:
            parameters["ChainUsed"].append(chain)
        print("Analysis will be done on chain(s) : " + str(parameters["ChainUsed"]) + " if they are available")

    if args.atoms is not None:
        parameters["AtomsUsed"] = args.atoms[0]
        print("Analysis will be done on atoms : " + str(parameters["AtomsUsed"]) + " if they are available")

    # simple or all analysis; until fluctuation and correlation can be calculated directly by mode files,
    # all analysis cannot be executed without all files needed.
    if args.all:
        if parameters["ProteinFile"]:
            parameters["AllAnalysis"] = True
        else:
            logging.debug("No protein file; only simple analysis will be done.")
            print("No protein file; only simple analysis will be done.")

    return parameters


def main():
    """
    Main function to use the program.

    In : Nothing.
    Do : Process the arguments and execute asked functions
    Out : Nothing.
    """
    parameters = get_parameters()

    if not os.path.exists("results/"):
        os.makedirs("results/")

    prot = mol.Molecule()
    if parameters["ProteinFile"]:
        prot.init_protein(parameters["ProteinFile"])

    if parameters["CorrelationFile"]:
        prot.init_correlation(parameters["CorrelationFile"])
        prot.correlation.plot_correlation_matrix()

        if parameters["AllAnalysis"]:
            prot.hide_close_pairs_atoms(parameters["DistMin"])
            prot.write_correlation_sticks_pml(prot.get_pairs_atoms_infos(
                chains_used=parameters["ChainUsed"],
                atoms_used=parameters["AtomsUsed"],
                selection=parameters["Threshold"][0],
                threshold_upper=parameters["Threshold"][1],
                threshold_lower=parameters["Threshold"][2]))

    if parameters["FluctuationFile"]:
        prot.init_fluctuation(parameters["FluctuationFile"])
        prot.fluctuation.plot_fluctuations()

        if parameters["AllAnalysis"]:
            dssp_result_path = "results/"
            dssp_result_path += "_".join(prot.pdb_protein.get_id().split("_")[:-1]) + "_DSSP" + ".dat"
            prot.calculate_secondary_structures(dssp_result_path)

            prot.fluctuation.plot_fluctuations_and_structure(prot.read_dssp_file(dssp_result_path))


if __name__ == '__main__':
    main()
