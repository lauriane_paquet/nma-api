#!/usr/bin/env bash

source activate nma-api-env
python src/main.py $@
source deactivate