#!/usr/bin/env bash

# in case user is in wrong folder
DIR="$( cd -P "$( dirname ${BASH_SOURCE[0]} )" && pwd )"
cd $DIR

# create virtual environment
conda env create -f environment.yml

